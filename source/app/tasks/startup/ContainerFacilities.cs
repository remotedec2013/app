﻿using app.utility.containers;
using System.Linq;

namespace app.tasks.startup
{
  public class ContainerFacilities
  {
    public static IPickAConstructor greediest_ctor_picker = (type) =>
    {
      return  type.GetConstructors().OrderByDescending(x => x.GetParameters().Length).First();
    };
  }
}