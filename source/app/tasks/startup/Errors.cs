﻿using System;
using app.utility.containers;
using app.web.core;

namespace app.tasks.startup
{
  public class Errors
  {
    public static ICreateTheExceptionForAMissingDependencyFactory factory_not_registered_for_type = (
      type_that_has_no_factory) =>
    {
      return
        new Exception(string.Format("There is no factory registered that can create an instance of a: #{0}",
                                    type_that_has_no_factory));
    };

    public static ICreateErrorsFromFactoryCreationErrors dependency_creation_error =
      (type_that_could_not_be_created, exception_thrown_by_container_framework) =>
      {
        return new Exception(string.Format("There was an error trying to create an instance of: #{0}",
                                           type_that_could_not_be_created.Name), exception_thrown_by_container_framework);
      };

    public static ICreateTheMissingCommand command_not_registered_for_request = (request) =>
    {
      throw new Exception("There is no command that can process the request");
    };
  }
}