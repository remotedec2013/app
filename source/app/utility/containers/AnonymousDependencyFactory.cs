﻿using System;

namespace app.utility.containers
{
  public class AnonymousDependencyFactory : ICreateOneDependency
  {
    Func<object> real_factory;

    public AnonymousDependencyFactory(Func<object> real_factory)
    {
      this.real_factory = real_factory;
    }

    public object create()
    {
      return real_factory();
    }
  }
}