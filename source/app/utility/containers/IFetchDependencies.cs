﻿using System;

namespace app.utility.containers
{
  public interface IFetchDependencies
  {
    Dependency a<Dependency>();
    object a(Type dependency_type);
  }
}