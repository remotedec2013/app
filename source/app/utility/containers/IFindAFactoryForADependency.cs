﻿using System;

namespace app.utility.containers
{
  public interface IFindAFactoryForADependency
  {
    ICreateOneDependency get_factory_to_create(Type dependency_to_create);
  }
}