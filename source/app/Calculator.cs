﻿using System;
using System.Data;
using System.Linq;
using System.Security;

namespace app
{
  public class Calculator : ICalculate
  {
    IDbConnection connection;

    public Calculator(IDbConnection connection)
    {
      this.connection = connection;
    }

    public int add(int first, int second)
    {
      ensure_all_positive(first, second);

      connection.Open();
      connection.CreateCommand().ExecuteNonQuery();

      return first + second;
    }

    public void shut_off()
    {
        if (!System.Threading.Thread.CurrentPrincipal.IsInRole("any"))
        {
            throw new SecurityException("not permitted");
        }
    }

    static void ensure_all_positive(params int[] values)
    {
      if (! values.All(x => x > 0)) throw new ArgumentException();
    }
  }
}