﻿namespace app.web.core
{
  public class FrontController : IProcessRequests
  {
    IFindCommands command_registry;

    public FrontController(IFindCommands command_registry)
    {
      this.command_registry = command_registry;
    }

    public void process(IContainRequestDetails a_new_controller_request)
    {
      var command_that_can_process = command_registry.get_the_command_that_can_process(a_new_controller_request);

      command_that_can_process.process(a_new_controller_request);
    }
  }
}