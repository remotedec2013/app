﻿namespace app.web.core
{
  public class WebCommand : IProcessOneRequest
  {
    IMatchARequest request_specification;
    IRunAFeature run_a_feature;

    public WebCommand(IMatchARequest request_specification, IRunAFeature run_a_feature)
    {
      this.request_specification = request_specification;
      this.run_a_feature = run_a_feature;
    }

    public void process(IContainRequestDetails request)
    {
      run_a_feature.process(request);
    }

    public bool can_process(IContainRequestDetails request)
    {
      return request_specification(request);
    }
  }
}