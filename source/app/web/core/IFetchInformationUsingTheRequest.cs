﻿namespace app.web.core
{
  public delegate ReportModel IFetchInformationUsingTheRequest<ReportModel>(IContainRequestDetails request);

  public interface IFetchAReportUsingARequest<ReportModel>
  {
    ReportModel fetch_using(IContainRequestDetails request);
  }
}