﻿using System.Collections.Generic;
using System.Linq;

namespace app.web.core
{
  public class CommandRegistry : IFindCommands
  {
    IEnumerable<IProcessOneRequest> all_commands;
    ICreateTheMissingCommand missing_command_factory;

    public CommandRegistry(IEnumerable<IProcessOneRequest> all_commands,
                           ICreateTheMissingCommand missing_command_factory)
    {
      this.all_commands = all_commands;
      this.missing_command_factory = missing_command_factory;
    }

    public IProcessOneRequest get_the_command_that_can_process(IContainRequestDetails request)
    {
      return all_commands.FirstOrDefault(x => x.can_process(request)) ?? missing_command_factory(request);
    }
  }
}