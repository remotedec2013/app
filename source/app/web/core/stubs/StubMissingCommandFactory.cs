﻿using System;

namespace app.web.core.stubs
{
  public class StubMissingCommandFactory
  {
    public static ICreateTheMissingCommand handler = x =>
    {
      throw new NotImplementedException("There is no command configured to run the request");
    }; 
  }
}