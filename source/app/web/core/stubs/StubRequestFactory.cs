﻿using System;
using System.Web;
using app.utility;
using app.web.core.aspnet;

namespace app.web.core.stubs
{
  public class StubRequestFactory : ICreateControllerRequests
  {
    public IContainRequestDetails create_request(HttpContext a_new_aspnet_request)
    {
      return Stub.with<StubRequest>();
    }

    class StubRequest : IContainRequestDetails
    {
      public InputModel map<InputModel>()
      {
        return Activator.CreateInstance<InputModel>();
      }
    }
  }
}