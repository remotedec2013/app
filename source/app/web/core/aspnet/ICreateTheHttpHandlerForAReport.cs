﻿using System.Web;

namespace app.web.core.aspnet
{
  public interface ICreateTheHttpHandlerForAReport
  {
    IHttpHandler create_view_to_display<Report>(Report report);
  }
}