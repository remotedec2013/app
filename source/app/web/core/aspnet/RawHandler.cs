﻿using System.Web;
using app.utility.containers;

namespace app.web.core.aspnet
{
  public class RawHandler : IHttpHandler
  {
    IProcessRequests front_controller;
    ICreateControllerRequests request_factory;

    public RawHandler():this(Dependencies.fetch.a<IProcessRequests>(), Dependencies.fetch.a<ICreateControllerRequests>())
    {
    }

    public RawHandler(IProcessRequests front_controller, ICreateControllerRequests request_factory)
    {
      this.front_controller = front_controller;
      this.request_factory = request_factory;
    }

    public void ProcessRequest(HttpContext context)
    {
      var new_request = request_factory.create_request(context);

      front_controller.process(new_request);
    }

    public bool IsReusable
    {
      get { return true; }
    }
  }
}