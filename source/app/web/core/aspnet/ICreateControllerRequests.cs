﻿using System.Web;

namespace app.web.core.aspnet
{
  public interface ICreateControllerRequests
  {
    IContainRequestDetails create_request(HttpContext a_new_aspnet_request);
  }
}