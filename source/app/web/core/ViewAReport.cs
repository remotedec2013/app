namespace app.web.core
{
  public class ViewAReport<ReportModel> : IRunAFeature
  {
    IDisplayInformation display_engine;
    IFetchInformationUsingTheRequest<ReportModel> query;

    public ViewAReport(IDisplayInformation display_engine, IFetchInformationUsingTheRequest<ReportModel> query)
    {
      this.display_engine = display_engine;
      this.query = query;
    }

    public void process(IContainRequestDetails request)
    {
      display_engine.display(query(request));
    }
  }
}