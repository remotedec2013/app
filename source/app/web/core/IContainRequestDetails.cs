﻿namespace app.web.core
{
  public interface IContainRequestDetails
  {
    InputModel map<InputModel>();
  }
}