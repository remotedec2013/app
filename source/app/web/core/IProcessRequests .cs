﻿namespace app.web.core
{
  public interface IProcessRequests 
  {
    void process(IContainRequestDetails a_new_controller_request);
  }
}