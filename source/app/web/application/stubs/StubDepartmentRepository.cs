﻿using System.Collections.Generic;
using System.Linq;

namespace app.web.application.stubs
{
  public class StubDepartmentRepository : IFindInformationInTheStore
  {
    public IEnumerable<DepartmentDisplayItem> get_the_main_departments()
    {
      return Enumerable.Range(1, 100)
                       .Select(x => new DepartmentDisplayItem {name = x.ToString("The Main Department 0")});
    }

    public IEnumerable<DepartmentDisplayItem> get_sub_departments(DepartmentDisplayItem sub_department)
    {
      return Enumerable.Range(1, 10)
                       .Select(x => new DepartmentDisplayItem {name = x.ToString("Sub Department 0")});
    }

    public IEnumerable<ProductDisplayItem> get_products_in(DepartmentDisplayItem selectedDepartment)
    {
      return Enumerable.Range(1, 100)
                       .Select(
                         x => new ProductDisplayItem
                         {
                           name = x.ToString("Product of a 0 " + selectedDepartment.name),
                           price = x
                         });
    }
  }
}
