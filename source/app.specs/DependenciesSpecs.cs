﻿using Machine.Specifications;
using app.utility.containers;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(Dependencies))]
  public class DependenciesSpecs
  {
    public abstract class concern : Observes
    {
    }

    public class when_providing_access_to_the_container_facade : concern
    {
      Establish c = () =>
      {
        the_container = fake.an<IFetchDependencies>();
        ISetupTheContainer setup = () => the_container;
        spec.change(() => Dependencies.container_factory).to(setup);
      };

      Because b = () =>
        result = Dependencies.fetch;

      It should_return_the_container_built_by_the_container_setup_process = () =>
        result.ShouldEqual(the_container);

      static IFetchDependencies result;
      static IFetchDependencies the_container;
    }
  }
}