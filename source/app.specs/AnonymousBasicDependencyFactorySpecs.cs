﻿using System;
using Machine.Specifications;
using app.utility.containers;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  [Subject(typeof(AnonymousBasicDependencyFactorySpecs))]
  public class AnonymousBasicDependencyFactorySpecs
  {
    public abstract class concern : Observes<ICreateOneDependency,
                                      AnonymousDependencyFactory>
    {
    }

    public class when_creating_a_dependency : concern
    {
      Establish c = () =>
      {
        var created = new object();
        depends.on<Func<object>>(() => created);
      };

      Because b = () =>
        result = sut.create();

      It should_return_the_dependency_created_by_its_provided_delegate = () =>
        result.ShouldEqual(42);

      static object result;
    }
  }
}