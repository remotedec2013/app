﻿using System;
using System.Linq.Expressions;
using Machine.Specifications;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
  public class ExpressionsSpecs
  {
    public abstract class concern : Observes
    {
    }

    public class when_building_an_expression_on_the_fly : concern
    {
      It should_create = () =>
      {
        Func<int, bool> is_even = (number) => number%2 == 0;

        var the_number_2 = Expression.Constant(2);
        var zero = Expression.Constant(0);
        var parameter = Expression.Parameter(typeof(int), "number");

        var modulus = Expression.Modulo(parameter, the_number_2);
        var equal_to_zero = Expression.Equal(modulus, zero);

        var lambda = Expression.Lambda<Func<int, bool>>(equal_to_zero, parameter);
        var final = lambda.Compile();

        final(2).ShouldBeTrue();
        final(3).ShouldBeFalse();
      };
    }
  }
}