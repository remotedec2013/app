﻿using Machine.Specifications;
using app.web.core;
using developwithpassion.specifications.rhinomocks;
using developwithpassion.specifications.extensions;

namespace app.specs.web
{
  [Subject(typeof(WebCommand))]
  public class WebCommandSpecs
  {
    public abstract class concern : Observes<IProcessOneRequest,
                                      WebCommand>
    {
    }

    public class when_determining_if_it_can_process_a_request : concern
    {
      Establish c = () =>
      {
        request = fake.an<IContainRequestDetails>();
        was_used = false;
        depends.on<IMatchARequest>(x =>
        {
          was_used = true;
          return x == request; 
        });
      };

      Because b = () =>
        result = sut.can_process(request);

      It decides_by_using_its_request_specification = () =>
      {
        was_used.ShouldBeTrue();
        result.ShouldBeTrue();
      };

      static bool result;
      static IContainRequestDetails request;
      static bool was_used;
    }

    public class when_processing_the_request : concern
    {
      Establish c = () =>
      {
        application_feature = depends.on<IRunAFeature>();
        request = fake.an<IContainRequestDetails>();
      };

      Because b = () =>
        sut.process(request);

      It delegates_the_processing_to_the_application_feature = () =>
        application_feature.received(x => x.process(request));

      static IRunAFeature application_feature;
      static IContainRequestDetails request;
    }
  }
}