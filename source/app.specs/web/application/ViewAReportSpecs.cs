﻿using Machine.Specifications;
using app.web.application;
using app.web.core;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace app.specs.web.application
{
  [Subject(typeof(ViewAReport<>))]
  public class ViewAReportSpecs
  {
    public abstract class concern : Observes<IRunAFeature,
                                      ViewAReport<int>>
    {
    }

    public class when_viewing_a_report : concern
    {
      Establish c = () =>
      {
        request = fake.an<IContainRequestDetails>();

        depends.on<IFetchInformationUsingTheRequest<int>>(x =>
        {
          x.ShouldEqual(request);
          return answer_to_life;
        });

        display_engine = depends.on<IDisplayInformation>();
        answer_to_life = 42;
      };

      Because b = () =>
        sut.process(request);

      It should_display_the_data_for_the_report = () =>
        display_engine.received(x => x.display(answer_to_life));

      static IContainRequestDetails request;
      static int report;
      static IDisplayInformation display_engine;
      static int answer_to_life;
    }
  }
}