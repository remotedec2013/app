﻿using System;
using System.Collections.Generic;
using System.Data;
using Machine.Specifications;
using app.utility.containers;
using developwithpassion.specifications.rhinomocks;

namespace app.specs.web.application
{
  [Subject(typeof(DependencyFactories))]
  public class DependencyFactoriesSpecs
  {
    public abstract class concern : Observes<IFindAFactoryForADependency,
                                      DependencyFactories>
    {
    }

    public class when_finding_the_factory_for_a_dependency : concern
    {
      public class and_it_has_the_factory
      {
        Establish context = () =>
        {
          factory = fake.an<ICreateOneDependency>();
          all_the_factories = new Dictionary<Type, ICreateOneDependency>();
          all_the_factories[typeof(IStub)] = factory;
          depends.on(all_the_factories);
        };

        Because b = () =>
          result = sut.get_factory_to_create(typeof(IStub));

        It should_return_the_factory_that_can_create_the_dependency = () =>
          result.ShouldEqual(factory);

        static ICreateOneDependency result;
        static ICreateOneDependency factory;
        static IDictionary<Type, ICreateOneDependency> all_the_factories;
      }

      public class and_it_does_not_have_the_factory
      {
        Establish context = () =>
        {
          created_exception = new Exception();
          factory = fake.an<ICreateOneDependency>();
          all_the_factories = new Dictionary<Type, ICreateOneDependency>();
          depends.on(all_the_factories);
          depends.on<ICreateTheExceptionForAMissingDependencyFactory>(x =>
          {
            x.ShouldEqual(typeof(IDbConnection));
            return created_exception;
          });
        };

        Because b = () =>
          spec.catch_exception(() => sut.get_factory_to_create(typeof(IDbConnection)));

        It should_throw_the_message_created_by_the_message_builder = () =>
          spec.exception_thrown.ShouldEqual(created_exception);

        static ICreateOneDependency factory;
        static IDictionary<Type, ICreateOneDependency> all_the_factories;
        static Exception created_exception;
      }
    }
  }

  public interface IStub
  {
  }
}