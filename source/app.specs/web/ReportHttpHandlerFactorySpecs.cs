﻿using System.Web;
using Machine.Specifications;
using app.web.core.aspnet;
using developwithpassion.specifications.rhinomocks;

namespace app.specs.web
{
  [Subject(typeof(ReportHttpHandlerFactory))]
  public class ReportHttpHandlerFactorySpecs
  {
    public abstract class concern : Observes<ICreateTheHttpHandlerForAReport,
                                      ReportHttpHandlerFactory>
    {
    }

    public class when_creating_the_handler_for_a_report : concern
    {
      Establish c = () =>
      {
        our_handler = fake.an<IDisplayA<NewReport>>();
        my_path_aspx = "my_path.aspx";

        depends.on<IFindThePathsToReportPages>(x =>
        {
          x.ShouldEqual(typeof(NewReport));
          return my_path_aspx;
        });

        depends.on<ICreateRawHttpHandlers>((path, type) =>
        {
          path.ShouldEqual(my_path_aspx);
          type.ShouldEqual(typeof(IDisplayA<NewReport>));
          return our_handler;
        });

        my_report = new NewReport();
      };

      Because b = () =>
        result = sut.create_view_to_display(my_report);

      It should_create_the_handler_that_can_render_the_data = () =>
        result.ShouldEqual(our_handler);

      It should_populate_the_view_with_its_data = () =>
        our_handler.report.ShouldEqual(my_report);

      static IHttpHandler result;
      static IDisplayA<NewReport> our_handler;
      static NewReport my_report;
      static string my_path_aspx;
    }

    public class NewReport
    {
    }
  }
}